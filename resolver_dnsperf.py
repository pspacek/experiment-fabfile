#!/usr/bin/python
from pathlib import Path
import datetime
import logging
import sys

import fabric

from remote import bind, certs, dumdumd, dnsperf, shotgun
from remote.flamegraph import FlameGraph
from remote.perf import PerfProfiler
import remote.connection


SCRIPT_PATH = Path(__file__).parent


def test():
    server = remote.connection.RemoteConnection("::1")
    server.config["run"]["echo"] = "1"
    server.config["run"]["hide"] = "both"

    client = remote.connection.RemoteConnection("::1")
    client.config["run"]["echo"] = "1"
    client.config["run"]["hide"] = "both"

    work_dir = Path("/tmp/srv_wrk")

    resolver = "dumdumd"
    resolver_dir = work_dir / str(resolver)
    build_dir = resolver_dir / "build"
    run_dir = resolver_dir / "run"

    perf_profile = True
    test_cfg = {
        "dot_port": 853,
        "doh_port": 8053,
        "dns_port": 5353,
        "ipaddr6": "::1",
        "refuse": True,
        "srv_threads": 1,
        "cert_type": "p256",
    }

    if resolver == "bind":
        test_cfg["ref"] = sys.argv[1]
        res_image = bind.Image(server, build_dir, ref=test_cfg["ref"])
        res_inst = bind.Resolver(res_image, run_dir)
    else:
        res_image = dumdumd.Image(server, build_dir)
        res_inst = dumdumd.Resolver(res_image, run_dir)
    res_image.image_prep()  # TODO: CFLAGS etc.

    res_inst.configure(**test_cfg)
    certs.copy("p256", server, res_inst.work_dir)
    res_inst.start()

    if perf_profile:
        # profiler run
        profiler = PerfProfiler(server, resolver_dir / "perf")
        profiler.record(pids=res_inst.pids)

    shotgun_image = shotgun.Image(client, work_dir / "shotgun" / "build")
    shotgun_image.image_prep()
    shotgun_inst = shotgun.Client(shotgun_image, work_dir / "shotgun" / "run")
    shotgun_inst.configure()

    dnsperf_image = dnsperf.Image(client, work_dir / "dnsperf" / "build")
    dnsperf_image.image_prep()
    dnsperf_inst = dnsperf.Client(dnsperf_image, work_dir / "dnsperf" / "run")
    dnsperf_inst.configure()

    # run test workload - normal traffic
    start_time = datetime.datetime.now()
    shotgun_inst.start(
        docker_params=["-v", "/mnt/experiment/onequery.pcap:/pcap"],
        args=[
            "replay.py",
            "--config",
            "udp",
            "--server",
            test_cfg["ipaddr6"],
            # "-T",  # TODO
            # "",
            "--dns-port",
            test_cfg["dns_port"],
            "--dot-port",
            test_cfg["dot_port"],
            "--doh-port",
            test_cfg["doh_port"],
            #'--bind-net',  # TODO
            # '',
            "--verbosity",
            "3",
            "--read",
            "/pcap",  # TODO with stdin
            "--outdir",
            shotgun_inst.work_dir / "output",
        ],
    )

    # run test workload - DoS traffic
    start_time = datetime.datetime.now()
    dnsperf_inst.start(
        args=[
            "-s",
            test_cfg["ipaddr6"],
            "-l",
            "2",
            "-c",
            "10",
            "-p",
            test_cfg["dns_port"],
            "-d",
            dnsperf_inst.work_dir / "qlist",
        ],
    )

    shotgun_inst.wait()
    dnsperf_inst.wait()
    stop_time = datetime.datetime.now()

    if perf_profile:
        profiler.stop()
        thread_profile = profiler.cut_and_symbolize(
            output_dir_name=Path("threads"), start=start_time, stop=stop_time
        )
        threaded_flameg = FlameGraph(thread_profile)
        threaded_flameg.summarize_stacks(separate_threads=True)
        threaded_flameg.plot()

        combined_profile = thread_profile.copy(profiler.work_dir / "combined")
        combined_flameg = FlameGraph(combined_profile)
        combined_flameg.summarize_stacks(separate_threads=False)
        res_inst.combine_perf_stacks(combined_flameg.stacks_path)
        combined_flameg.plot()


if __name__ == "__main__":
    test()
