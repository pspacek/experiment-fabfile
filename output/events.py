#!/usr/bin/python
import datetime
import json
import time

from typing import Optional


class EventLog:
    def __init__(self, outpath):
        self.outfile = open(outpath, "w")

    def log(self, msg: str, timestamp: Optional[datetime.datetime] = None):
        if not timestamp:
            ns = time.clock_gettime_ns(time.CLOCK_REALTIME)
        else:
            ns = (  # float would lose precision
                int(timestamp.timestamp()) * 1_000_000_000
                + timestamp.microsecond * 1000
            )
        event = {"type": "event", "time": ns, "msg": msg}
        json.dump(event, self.outfile)
        self.outfile.write("\n")
