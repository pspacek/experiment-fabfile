#!/usr/bin/bash
set -o nounset -o errexit -o xtrace

docker ps -aq | xargs --no-run-if-empty docker rm -f
: excercise image rebuild
: docker rmi --no-prune --all --force
rm -rf /tmp/srv_wrk

"$@"

#tree /tmp/srv_wrk
kitty +kitten icat /tmp/srv_wrk/*/perf/*/output.svg
