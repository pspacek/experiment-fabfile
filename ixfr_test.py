#!/usr/bin/python
import argparse
from pathlib import Path
import datetime
import logging
import json
import time
import sys

import fabric
import requests.exceptions

from output import events
from remote import bind, certs, dumdumd, dnsperf, shotgun
from remote import docker
from remote import resmon
from remote.flamegraph import FlameGraph
from remote.perf import PerfProfiler
import remote.connection


SCRIPT_PATH = Path(__file__).parent


def test(test_cfg):
    pri_profile = test_cfg["hosts"]["primary"]["perf_profile"]
    sec_profile = test_cfg["hosts"]["secondary"]["perf_profile"]

    pri_resmon_cfg = test_cfg["hosts"]["primary"]["resmon"]
    sec_resmon_cfg = test_cfg["hosts"]["secondary"]["resmon"]

    pri_c = remote.connection.RemoteConnection(
        test_cfg["hosts"]["primary"]["ssh_destination"]
    )
    pri_c.config["run"]["echo"] = "1"
    pri_c.config["run"]["hide"] = "both"

    sec_c = remote.connection.RemoteConnection(
        test_cfg["hosts"]["secondary"]["ssh_destination"]
    )
    sec_c.config["run"]["echo"] = "1"
    sec_c.config["run"]["hide"] = "both"

    work_dir = Path(test_cfg["work_dir"])

    pri_dir = work_dir / "primary"
    pri_dir.mkdir(parents=True)
    pri_eventlog = events.EventLog(pri_dir / "testevents.json")

    pri_build_dir = pri_dir / "build"
    pri_run_dir = pri_dir / "run"

    pri_image = bind.Image(pri_c, pri_build_dir, ref=test_cfg["version"])
    pri_inst = bind.Primary(pri_image, pri_run_dir, "primary")
    pri_image.image_prep()  # TODO: CFLAGS etc.

    # determine Cgroup path
    cgroup_test = docker.AsyncDockerContainer(
        pri_image, work_dir / "cgroup_test", "cgroup_test"
    )
    cgroup_test.configure()
    cgroup_test.start(["sleep", "365d"])
    cgroup_state = cgroup_test.inspect()
    cgroup_path = Path(cgroup_state["State"]["CgroupPath"])
    assert cgroup_path.parts[0] == "/", (
        "unexpected relative Cgroup path",
        cgroup_state,
    )
    assert cgroup_path.parts[-2] == "cgroup_test.slice", (
        "unexpected Cgroup slice",
        cgroup_state,
    )
    cgroup_parent_path = Path(*cgroup_path.parts[1:-2])
    # TODO: cgroup_test.stop()
    ### ^^^ move out

    pri_inst.configure(
        ipaddr6=test_cfg["hosts"]["primary"]["ip"],
        tld_ch_path=test_cfg["hosts"]["primary"]["tld_ch_path_old"],
        **test_cfg,
    )

    if pri_resmon_cfg:
        pri_resmon = resmon.Resmon(pri_c, pri_dir / "resmon")
        pri_resmon.prepare()
        pri_resmon.configure(
            bind_stats_url=f"http://[{test_cfg['hosts']['primary']['ip']}]:{test_cfg['stats_port']}/json/v1/net"
        )
        pri_resmon.record(Path("/sys/fs/cgroup") / cgroup_parent_path / "primary.slice")

    pri_inst.start()
    pri_eventlog.log(timestamp=pri_inst.started_at, msg="starting")

    if pri_profile:
        pri_profiler = PerfProfiler(pri_c, pri_dir / "perf")
        pri_profiler.record(pids=pri_inst.pids)

    # TODO abstract this
    pri_running_log = pri_inst.log_parse(pri_inst.wait_for_line("running$").stdout)
    pri_running_time = pri_running_log["time"]
    pri_eventlog.log(timestamp=pri_running_time, msg="ready")
    print('time to "running" message', pri_running_time - pri_inst.started_at)

    if pri_profile:
        pri_profiler.stop()
        thread_profile = pri_profiler.cut_and_symbolize(
            output_dir_name=Path("threads"),
            start=pri_inst.started_at,
            stop=pri_running_time,
        )
        threaded_flameg = FlameGraph(thread_profile)
        threaded_flameg.summarize_stacks(separate_threads=True)
        threaded_flameg.plot()

        combined_profile = thread_profile.copy(pri_profiler.work_dir / "combined")
        combined_flameg = FlameGraph(combined_profile)
        combined_flameg.summarize_stacks(separate_threads=False)
        pri_inst.combine_perf_stacks(combined_flameg.stacks_path)
        combined_flameg.plot()

    sec_dir = work_dir / "secondary"
    sec_dir.mkdir()
    sec_eventlog = events.EventLog(sec_dir / "testevents.json")

    sec_build_dir = sec_dir / "build"
    sec_run_dir = sec_dir / "run"

    if sec_resmon_cfg:
        sec_resmon = resmon.Resmon(sec_c, sec_dir / "resmon")
        sec_resmon.prepare()

    sec_image = bind.Image(sec_c, sec_build_dir, ref=test_cfg["version"])
    sec_image.image_prep()

    if sec_resmon_cfg:
        sec_resmon.configure(
            f"http://[{test_cfg['hosts']['secondary']['ip']}]:{test_cfg['stats_port']}/json/v1/net"
        )
        sec_resmon.record(
            Path("/sys/fs/cgroup") / cgroup_parent_path / "secondary.slice"
        )
    sec_inst = bind.Secondary(sec_image, sec_run_dir, "secondary")

    sec_inst.configure(
        ipaddr6=test_cfg["hosts"]["secondary"]["ip"],
        primary_ip=test_cfg["hosts"]["primary"]["ip"],
        **test_cfg,
    )
    sec_inst.start()

    if sec_profile:
        sec_profiler = PerfProfiler(sec_c, sec_dir / "perf")
        sec_profiler.record(pids=sec_inst.pids)

    sec_eventlog.log(timestamp=sec_inst.started_at, msg="starting")

    while True:
        try:
            sec_load_time = bind.wait(
                f"http://[{test_cfg['hosts']['secondary']['ip']}]:{test_cfg['stats_port']}/json/v1",
                {"secondary"},
                test_cfg["zone_count"],
            )
            break
        except (RuntimeError, requests.exceptions.ConnectionError) as ex:
            print(ex)
            time.sleep(10)
    print("primary loaded all zones in", pri_running_time - pri_inst.started_at)
    print("secondary loaded all zones in", sec_load_time - sec_inst.started_at)
    sec_eventlog.log(timestamp=sec_load_time, msg="axfr done")

    if pri_resmon_cfg:
        pri_resmon.stop()

    if cfg["tld_ch"] == "ixfr":
        pri_inst.stop()

        pri_latest_dir = work_dir / "primary-latest"
        pri_latest_run_dir = pri_latest_dir / "run"
        pri_latest_run_dir.mkdir(parents=True)
        pri_latest_inst = bind.Primary(pri_image, pri_latest_run_dir, "primary-latest")

        pri_latest_inst.configure(
            ipaddr6=test_cfg["hosts"]["primary"]["ip"],
            notify_ip=test_cfg["hosts"]["secondary"]["ip"],
            tld_ch_path=test_cfg["hosts"]["primary"]["tld_ch_path_latest"],
            **test_cfg,
        )

        if pri_resmon_cfg:
            pri_latest_resmon = resmon.Resmon(pri_c, pri_latest_dir / "resmon")
            pri_latest_resmon.prepare()
            pri_latest_resmon.configure(
                bind_stats_url=f"http://[{test_cfg['hosts']['primary']['ip']}]:{test_cfg['stats_port']}/json/v1/net"
            )
            pri_latest_resmon.record(
                Path("/sys/fs/cgroup") / cgroup_parent_path / "primary-latest.slice"
            )

        pri_latest_inst.start()
        pri_latest_eventlog = events.EventLog(pri_latest_dir / "testevents.json")
        pri_latest_eventlog.log(timestamp=pri_latest_inst.started_at, msg="starting")

        pri_latest_running_log = pri_inst.log_parse(
            pri_latest_inst.wait_for_line("running$").stdout
        )
        pri_latest_eventlog.log(timestamp=pri_latest_running_log["time"], msg="ready")
        print(
            'time to "running" message - latest zone',
            pri_latest_running_log["time"] - pri_latest_inst.started_at,
        )

        while sec_load_time < pri_latest_running_log["time"]:
            print(sec_load_time, "<", pri_latest_running_log["time"])
            try:
                sec_load_time = bind.wait(
                    f"http://[{test_cfg['hosts']['secondary']['ip']}]:{test_cfg['stats_port']}/json/v1",
                    {"secondary"},
                    test_cfg["zone_count"],
                )
            except (RuntimeError, requests.exceptions.ConnectionError) as ex:
                print(ex)
                time.sleep(10)
        sec_eventlog.log(timestamp=sec_load_time, msg="ixfr done")
        print("ixfr done in", sec_load_time - pri_latest_running_log["time"])

    if sec_resmon_cfg:
        sec_resmon.stop()

    if sec_profile:
        sec_profiler.stop()
        thread_profile = sec_profiler.cut_and_symbolize(
            output_dir_name=Path("threads"),
            start=sec_inst.started_at,
            stop=sec_inst.started_at,
        )
        threaded_flameg = FlameGraph(thread_profile)
        threaded_flameg.summarize_stacks(separate_threads=True)
        threaded_flameg.plot()

        combined_profile = thread_profile.copy(sec_profiler.work_dir / "combined")
        combined_flameg = FlameGraph(combined_profile)
        combined_flameg.summarize_stacks(separate_threads=False)
        sec_inst.combine_perf_stacks(combined_flameg.stacks_path)
        combined_flameg.plot()

    pri_c.get(str(pri_inst.log_path), str(pri_run_dir / "log"))
    sec_c.get(str(sec_inst.log_path), str(sec_run_dir / "log"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config",
        help="JSON file with most test variables",
        type=argparse.FileType("r"),
        required=True,
    )
    parser.add_argument(
        "--version", help="BIND version to test (Git reference)", required=True
    )
    parser.add_argument(
        "--small-zones",
        help="number of minimal zones to transfer",
        type=int,
        required=True,
    )
    parser.add_argument(
        "--tld",
        help=(
            "Configure CH. TLD using data files already present on the primary, "
            "possibly with primary restarted without and with journal"
        ),
        choices={"axfr", "ixfr"},
    )
    parser.add_argument(
        "--debug",
        action="store_true",
    )
    parser.add_argument(
        "--tsig",
        action="store_true",
        help="require TSIG for all communication between nodes",
    )
    parser.add_argument(
        "--xot",
        action="store_true",
        help="require unauthenticated XoT for all communication between nodes",
    )

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logging.info("%s", args)
    cfg = json.load(args.config)
    cfg["version"] = args.version
    cfg["xot"] = args.xot
    cfg["zone_count"] = args.small_zones

    if args.tsig:
        cfg["tsig_ips"] = list(host["ip"] for host in cfg["hosts"].values())

    cfg["tld_ch"] = args.tld

    logging.info("%s", cfg)
    test(cfg)
