#!/usr/bin/python
from pathlib import Path
import datetime
import io
import logging
import sys

import fabric

from remote.args import Inject
from remote import bind, certs, dumdumd, dnsperf
from remote.flamegraph import FlameGraph
from remote.perf import PerfProfiler
import remote.connection


SCRIPT_PATH = Path(__file__).parent


def test():
    server = remote.connection.RemoteConnection(
        "root@server", connect_kwargs={"compress": True}
    )
    server.config["run"]["echo"] = "1"
    server.config["run"]["hide"] = False

    client = remote.connection.RemoteConnection(
        "root@client", connect_kwargs={"compress": True}
    )
    client.config["run"]["echo"] = "1"
    client.config["run"]["hide"] = False

    work_dir = Path("/tmp/srv_wrk")
    server.runargs(["rm", "-rfv", work_dir])

    resolver = "dumdumd"
    resolver_dir = work_dir / str(resolver)
    build_dir = resolver_dir / "build"
    run_dir = resolver_dir / "run"

    perf_profile = True
    test_cfg = {
        "dot_port": False,
        "doh_port": False,
        "dns_port": 53,
        "ipaddr6": "2600:1f18:634c:d17e:9247:89f6:a71d:be0",  # TODO
        "refuse": True,
        "srv_threads": 16,
        "cert_type": "p256",
    }
    assert test_cfg["srv_threads"] > 0

    server_ncpus = server.runargs(
        [
            "find",
            "/sys/devices/system/cpu",
            "-mindepth",
            "1",
            "-maxdepth",
            "1",
            "-type",
            "d",
            "-name",
            "cpu[0-9]*",
            Inject("|"),
            "wc",
            "-l",
        ]
    )
    server_ncpus = int(server_ncpus.stdout)
    assert test_cfg["srv_threads"] <= server_ncpus

    server.runargs(["ethtool", "-L", "ens5", "combined", test_cfg["srv_threads"]])
    server.runargs(["ethtool", "-l", "ens5"])

    # keep CPU 0 always enabled
    server.runargs(
        ["tee"]
        + list(
            f"/sys/devices/system/cpu/cpu{idx}/online"
            for idx in range(1, test_cfg["srv_threads"])
        ),
        in_stream=io.StringIO("1"),
    )

    server.runargs(
        ["tee"]
        + list(
            f"/sys/devices/system/cpu/cpu{idx}/online"
            for idx in range(test_cfg["srv_threads"], server_ncpus)
        ),
        in_stream=io.StringIO("0"),
    )

    if resolver == "bind":
        test_cfg["ref"] = "each-qpcache-heavy"
        res_image = bind.Image(server, build_dir, ref=test_cfg["ref"])
        res_inst = bind.Resolver(res_image, run_dir)
    else:
        res_image = dumdumd.Image(server, build_dir)
        res_inst = dumdumd.Resolver(res_image, run_dir)
    res_image.image_prep()  # TODO: CFLAGS etc.

    res_inst.configure(**test_cfg)
    certs.copy("p256", server, res_inst.work_dir)
    res_inst.start()

    if perf_profile:
        # profiler run
        profiler = PerfProfiler(server, resolver_dir / "perf")
        profiler.record(pids=res_inst.pids)

    # dnsperf_image = dnsperf.Image(client, work_dir / "dnsperf" / "build")
    # dnsperf_image.image_prep()
    # dnsperf_inst = dnsperf.Client(dnsperf_image, work_dir / "dnsperf" / "run")
    # dnsperf_inst.configure()

    # run test workload - DoS traffic
    start_time = datetime.datetime.now()
    # dnsperf_inst.start(
    #    args=[
    #        "-s",
    #        test_cfg["ipaddr6"],
    #        "-l",
    #        "10",
    #        "-c",
    #        "256",
    #        "-T",
    #        "8",
    #        "-p",
    #        test_cfg["dns_port"],
    #        "-d",
    #        dnsperf_inst.work_dir / "qlist",
    #    ],
    # )
    #
    # dnsperf_inst.wait()
    result = client.runargs(["dig", f'@{test_cfg["ipaddr6"]}', "bla.blog.root.cz", "A"])
    print(result)

    result = client.runargs(
        [
            "sudo",
            "kxdpgun",
            "-i",
            "/tmp/qlist",
            "-t",
            "30",
            "-Q",
            "1000000",
            test_cfg["ipaddr6"],
        ]
    )
    stop_time = datetime.datetime.now()

    if perf_profile:
        profiler.stop()
        thread_profile = profiler.cut_and_symbolize(
            output_dir_name=Path("threads"), start=start_time, stop=stop_time
        )
        threaded_flameg = FlameGraph(thread_profile)
        threaded_flameg.summarize_stacks(separate_threads=True)
        threaded_flameg.plot()

        combined_profile = thread_profile.copy(profiler.work_dir / "combined")
        combined_flameg = FlameGraph(combined_profile)
        combined_flameg.summarize_stacks(separate_threads=False)
        res_inst.combine_perf_stacks(combined_flameg.stacks_path)
        combined_flameg.plot()

    print(result)


if __name__ == "__main__":
    test()
