#!/usr/bin/python
import argparse
import sys


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("records", type=int, help="number of records to generate")
    parser.add_argument("--rrtext", type=str, default="A\t192.0.2.1")
    parser.add_argument(
        "--owner", type=str, default="r{rec_idx:09d}", help="owner name template"
    )
    parser.add_argument("--appendto", type=str)
    args = parser.parse_args()

    outfile = sys.stdout
    if args.appendto:
        outfile = open(args.appendto, "a")

    for rec_idx in range(1, args.records + 1):
        print(
            "{owner}\t{rrtext}".format(
                owner=args.owner.format(rec_idx=rec_idx), rrtext=args.rrtext
            ),
            file=outfile,
        )


if __name__ == "__main__":
    main()
