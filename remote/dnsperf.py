from pathlib import Path
from io import StringIO

from .docker import DockerImage, AsyncDockerContainer


class Image(DockerImage):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("git_repos", ["https://github.com/DNS-OARC/dnsperf.git"])
        kwargs.setdefault("img_local_reg", "localhost/dnsperf")
        kwargs.setdefault("ref", "fb1944057ae4d197cb76c332f1a1f8e28370c38c")
        super().__init__(*args, **kwargs)
        self.git_dir = self.work_dir


class Client(AsyncDockerContainer):
    def configure(self, **kwargs):
        super().configure(**kwargs)
        self.conn.put(StringIO(". NS\n"), str(self.work_dir / "qlist"))
