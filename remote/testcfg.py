from pathlib import Path

import yaml

def load(filename: Path):
    with open(filename) as incfg:
        data = yaml.safe_load(incfg)

    
