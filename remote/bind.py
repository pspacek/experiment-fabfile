from pathlib import Path

from datetime import datetime
import logging
import requests

from .args import qpath
from .docker import DockerImage, AsyncDockerContainer
from . import certs


class Image(DockerImage):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault(
            "git_repos", ["https://gitlab.isc.org/isc-projects/bind9.git"]
        )
        kwargs.setdefault("img_local_reg", "localhost/bind")
        super().__init__(*args, **kwargs)
        self.git_dir = self.work_dir / "bind-git"

    def build_prep(self):
        self.conn.put_render_j2(
            Path("bind/Dockerfile.arch.j2"), self.work_dir / "Dockerfile"
        )


class BIND(AsyncDockerContainer):
    defaults = {
        "refuse": False,
        "bind_extra_opt": "",
    }

    def configure(self, **kwargs):
        super().configure(**kwargs)
        self.conn.put_render_j2(
            self.params["conf_template"], self.work_dir / "named.conf", self.params
        )
        if self.params.get("dot_port") or self.params.get("doh_port"):
            certs.copy(self.params["cert_type"], self.conn, self.work_dir)

    def start(self):
        super().start(
            args=[
                "/usr/sbin/named",
                "-g",
                "-c",
                "named.conf",
                "-n",
                self.params["srv_threads"],
                "-D",
                self.name,
            ],
        )

    def combine_perf_stacks(self, path: Path):
        self.conn.runargs(
            [
                "sed",
                "-e",
                r"s/^isc-\([^ 0-9-]*\)-\?[0-9]*;/\1;/",
                "-i",
                qpath(path),
            ]
        )


class Resolver(BIND):
    defaults = {
        "bind_recursive_clients": 10000,
        "bind_tcp_listen_queue": 512,
        "bind_tcp_clients": 2000000,
        "bind_max_cache_size": 10 * 1024**3,  # 10 GiB
        "bind_extra_opt": "",
        "conf_template": Path("bind/resolver.conf.j2"),
    }


class Primary(BIND):
    defaults = {"conf_template": Path("bind/primary.conf.j2")}

    def configure(self, **kwargs):
        super().configure(**kwargs)
        zfile = str(self.work_dir / "shared.zone")
        self.conn.put("zones/empty.zone", zfile)
        self.conn.put("zones/rrgen.py", str(self.work_dir))
        self.conn.runargs(
            [
                "python",
                self.work_dir / "rrgen.py",
                "--appendto",
                zfile,
                self.params["rrs_per_zone"],
            ]
        )
        if self.params.get("tld_ch_path"):
            self.start_prefix.extend(
                [
                    "--volume",
                    f'{self.params["tld_ch_path"]}:{self.params["tld_ch_path"]}:ro',
                ]
            )


class Secondary(BIND):
    defaults = {"conf_template": Path("bind/secondary.conf.j2")}


def fromiso(text):
    return datetime.fromisoformat(text)


def wait(statsurl, zonetypes, zcount):
    raw = requests.get(f"{statsurl}/server")
    try:
        data = raw.json()
    except requests.exceptions.JSONDecodeError as ex:
        logging.error("%s: %s", ex, raw)
        raise RuntimeError from ex

    xfrs_done = data["zonestats"].get("XfrSuccess", 0)
    if xfrs_done < zcount:
        raise RuntimeError("not enough XFRs yet", xfrs_done)

    data = requests.get(f"{statsurl}/zones").json()
    # hack - loadtime has only 1-second precision
    boottime = fromiso(data["boot-time"]).replace(microsecond=0)
    last_loaded = boottime
    unfinished_cnt = 0
    for view in data["views"]:
        for zone in data["views"][view]["zones"]:
            if zone["type"] not in zonetypes:
                continue
            loaded = fromiso(zone["loaded"])
            if loaded < boottime:
                unfinished_cnt += 1
            if loaded > last_loaded:
                last_loaded = loaded

    if unfinished_cnt > 0:
        raise RuntimeError("zones not loaded yet", unfinished_cnt)
    assert last_loaded, "no loaded zone of given type found"
    return last_loaded
