import datetime
import json
from pathlib import Path
import tempfile
from typing import List, Optional
import yaml

from .args import cmdjoin, qpath
from . import git

VERSION = "c63a725c3b9dd297f9473b7cba0487ec97544999"
REPO_URL = "https://gitlab.isc.org/isc-projects/resource-monitor.git"


class Resmon:
    def __init__(self, conn, work_dir: Path):
        self.conn = conn
        self.work_dir = work_dir
        self.git_dir = work_dir / "git"
        self.run_dir = work_dir / "run"
        self.async_run = None

    def prepare(self):
        self.conn.runargs(["rm", "-rf", self.work_dir])
        self.conn.runargs(["mkdir", "-p", self.work_dir])
        repo = git.RemoteGit(self.conn)
        repo.clone(self.git_dir, REPO_URL, VERSION)
        self.conn.runargs(["mkdir", "-p", self.run_dir])

    def configure(self, bind_stats_url):
        with tempfile.TemporaryDirectory() as tempdir:
            tmpconfpath = Path(tempdir) / "resmon.yaml"
            self.conn.get(str(self.git_dir / "resmon.yaml"), str(tmpconfpath))
            with open(tmpconfpath) as infile:
                resconf = yaml.safe_load(infile)
            resconf["cgroup_files"] = list(
                filter(
                    lambda item: item.get("name", "") != "io.stat",
                    resconf.get("cgroup_files", []),
                )
            )

            if not bind_stats_url:
                resconf["http_urls"] = list(
                    filter(
                        lambda item: item.get("parser", "") != "BINDJSONStat",
                        resconf.get("http_urls", []),
                    )
                )
            else:
                for url_cfg in resconf["http_urls"]:
                    if url_cfg.get("parser") != "BINDJSONStat":
                        continue
                    url_cfg["url"] = bind_stats_url
                    url_cfg["interval"] = 1
            with open(tmpconfpath, "w") as outfile:
                yaml.dump(resconf, outfile)
            self.conn.put(str(tmpconfpath), str(self.git_dir / "resmon.yaml"))

    def record(self, cgroup_base_dir):
        assert (
            not self.async_run
        ), "resmon object is not reusable for multiple recordings"
        cmd = [
            self.git_dir / "monitor.py",
            "--cgroup-base-dir",
            cgroup_base_dir,
            "--cgroup-glob",
            "*.scope",
        ]
        self.async_run = self.conn.async_runargs(self.run_dir, cmd)

    def stop(self):
        assert self.async_run
        self.async_run.stop()
