from .args import cmdjoin


class RemoteFS:
    def __init__(self, conn):
        self.conn = conn

    def rm_dir(self, dir_to_remove):
        self.conn.run(cmdjoin(["rm", "-rf", dir_to_remove]))
