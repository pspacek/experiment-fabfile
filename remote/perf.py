import datetime
import json
from pathlib import Path
from typing import List, Optional

from .args import cmdjoin, qpath


class PerfProfiler:
    record_opts = [
        "-F297",
        "--user-callchains",
        "-g",
        "--call-graph=dwarf,65528",
        "--debuginfod=https://debuginfod.elfutils.org/"
    ]

    def __init__(self, conn, work_dir: Path):
        self.conn = conn
        self.work_dir = work_dir
        self.data_file = work_dir / "perf.data"
        self.pids = []
        self.determine_time_offsets()

    def determine_time_offsets(self):
        """
        Determine relation between wall clock time and timestamps used by perf.
        This serves as poor man's heuristics for time range limit
        via perf script --time parameter.
        """
        ret = self.conn.sudo(
            "perf trace --time -e write date +%s.%N",
        )

        # Now we have output like this (first from date, second from perf trace):
        # 1697532044.002227206
        # 60856298.266 ( 0.015 ms): date/93363 write(fd: 1, buf: 0x558c42be37d0, count: 21) = 21
        self.timestamp_unix = float(ret.stdout.strip())  # from date
        self.timestamp_perf = float(ret.stderr.split()[0]) / 1000  # convert ms -> s
        self.unix_perf_offset = self.timestamp_unix - self.timestamp_perf

    def perf_to_datetime(self, perftime: float) -> datetime.datetime:
        ts = datetime.datetime.utcfromtimestamp(perftime + self.unix_perf_offset)
        ts.replace(tzinfo=datetime.timezone.utc)  # unix time is always in UTC
        return ts

    def datetime_to_perf(self, ts: datetime.datetime):
        return ts.timestamp() - self.unix_perf_offset

    def record(self, pids: List[int]):
        assert pids
        # object is not reusable for multiple recordings
        assert not self.pids
        self.pids = pids

        self.conn.runargs(["rm", "-rf", self.work_dir])
        self.conn.runargs(["mkdir", "-p", self.work_dir])
        self.async_run = self.conn.async_runargs(
            self.work_dir,
            [
                "sudo",
                "perf",
                "record",
            ]
            + self.record_opts
            + [
                "--pid",
                ",".join(str(pid) for pid in pids),
                "--output",
                self.work_dir / self.data_file,
            ],
        )

    def stop(self):
        self.async_run.stop()

    def cut_and_symbolize(
        self,
        output_dir_name: Path,
        start: Optional[datetime.datetime] = None,
        stop: Optional[datetime.datetime] = None,
    ):
        assert len(output_dir_name.parts) == 1, "must be a single filename"
        output_dir = self.work_dir / output_dir_name
        output_dir_name = output_dir / "script"
        self.conn.runargs(["mkdir", "-p", output_dir])
        cmd = [
            "perf",
            "script",
            "--force",  # symbolizer needs to run under root, so ownership might not match
            "-i",
            self.data_file,
            "-F",
            # list of fields used by stackcollapse-perf.pl commit d9fcc272b6a08c3e3e5b7919040f0ab5f8952d65
            "comm,pid,tid,time,event,ip,sym,dso,trace",
        ]

        if start or stop:
            time_spec = {}
            for name, value in [("start", start), ("stop", stop)]:
                if value:
                    time_spec[name] = f"{self.datetime_to_perf(value):.6f}"
                else:
                    time_spec[name] = ""

            time_limit = f"{time_spec['start']},{time_spec['stop']}"
            cmd += ["--time", time_limit]

        self.conn.sudo(f"{cmdjoin(cmd)} > {qpath(output_dir_name)}")
        return PerfProfile(self.conn, output_dir, output_dir_name)


class PerfProfile:
    """a finished Perf profile in human-readable form"""

    def __init__(self, conn, work_dir: Path, script_path: Path):
        self.conn = conn
        self.work_dir = work_dir
        self.script_path = script_path

    def copy(self, new_work_dir):
        self.conn.runargs(["mkdir", "-p", new_work_dir])
        script_copy = new_work_dir / self.script_path.name
        self.conn.runargs(["cp", "-rv", self.script_path, script_copy])
        return PerfProfile(self.conn, new_work_dir, script_copy)
