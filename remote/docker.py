import copy
import datetime
import functools
import inspect
import json
import logging
from pathlib import Path
from typing import List, TYPE_CHECKING

import invoke.runners
from invoke.exceptions import UnexpectedExit

from .args import cmdjoin

if TYPE_CHECKING:
    from .connection import RemoteConnection


class RemoteDocker:
    def __init__(self, conn):
        self.conn = conn

    def image_available(self, repo, tag):
        try:
            self.conn.runargs(["docker", "image", "inspect", f"{repo}:{tag}"])
            return True
        except UnexpectedExit:
            return False


class DockerImage:
    def __init__(
        self,
        conn: "RemoteConnection",
        work_dir: Path,
        git_repos: List[str],
        ref: str,
        img_local_reg: str,
        img_remote_reg=None,
    ):
        self.conn = conn
        self.img_local_reg = img_local_reg
        self.img_remote_reg = img_remote_reg
        self.git_repos = git_repos
        self.version = ref
        self.work_dir = Path(work_dir)
        self.git_dir = self.work_dir / "git"

    def git_clone(self):
        self.tag_or_commit = self.conn.git.clone_and_describe(
            self.git_dir, self.git_repos, self.version
        )
        return self.tag_or_commit

    def build_prep(self):
        pass

    def build(self):
        self.build_prep()
        self.conn.runargs(
            [
                "docker",
                "build",
                self.work_dir,
                "--tag",
                f"{self.img_local_reg}:{self.tag_or_commit}",
            ]
        )

    def pull(self, tag: str):
        assert self.img_remote_reg
        self.conn.runargs(["docker", "pull", f"{self.img_remote_reg}:{tag}"])
        self.conn.runargs(
            [
                "docker",
                "tag",
                f"{self.img_remote_reg}:{tag}",
                f"{self.img_local_reg}:{tag}",
            ]
        )

    def image_prep(self):
        tried = None
        if self.conn.docker.image_available(self.img_local_reg, self.version):
            self.tag_or_commit = self.version
            return

        if self.img_remote_reg:
            try:
                self.pull(self.version)
                self.tag_or_commit = self.version
                return
            except UnexpectedExit:
                logging.info(
                    "image pull has failed, going to canonicalize input version"
                )
        tried = self.version

        # canonicalize branch name
        retry_tag = self.git_clone()
        if retry_tag != tried:  # in case input was commit or tag don't retry
            if self.conn.docker.image_available(self.img_local_reg, retry_tag):
                self.tag_or_commit = retry_tag
                return

            if self.img_remote_reg:
                try:
                    self.pull(retry_tag)
                    self.tag_or_commit = retry_tag
                    return
                except UnexpectedExit:
                    logging.info("image pull has failed, going to build")

        self.build()
        assert self.conn.docker.image_available(self.img_local_reg, self.tag_or_commit)


class Defaults:
    defaults = {}

    def configure(self, **kwargs):
        """Walk through tree of parents and inherit their defaults"""
        self.params = kwargs

        for super_class in inspect.getmro(type(self)):
            try:
                for key, value in super_class.defaults.items():
                    self.params.setdefault(key, copy.deepcopy(value))
            except AttributeError:
                # skip over parent classes which do not specify defaults
                pass


class DockerContainer(Defaults):
    def __init__(self, image: DockerImage, work_dir: Path, name: str):
        self.image = image
        self.name = name
        self.work_dir = work_dir
        self.conn = image.conn
        self.start_prefix = [
            "docker",
            "run",
            "--name",
            self.name,
            "--cgroup-parent",
            f"{self.name}.slice",
            "--network",
            "host",
            "--log-driver",
            "json-file",
            "--volume",
            f"{self.work_dir}:{self.work_dir}",
            "--workdir",
            self.work_dir,
            "--pid",  # TODO
            "host",
            "--ipc",
            "host",
            "--uts",
            "host",
            "--security-opt",
            "seccomp=unconfined",
        ]
        self.ran_args = []
        self.ran_cmd = ""

    def configure(self, **kwargs):
        super().configure(**kwargs)
        self.conn.runargs(["mkdir", "-pv", self.work_dir])
        self.params.setdefault("work_dir", self.work_dir)

    def start(self, args, docker_params=[]):
        image_spec = f"{self.image.img_local_reg}:{self.image.tag_or_commit}"
        self.ran_args = self.start_prefix + docker_params + [image_spec] + args
        self.ran_cmd = self.conn._prep_args(self.ran_args)
        return self.conn.run(self.ran_cmd)

    @property
    def logs(self):
        pass

    def log_parse(self, line: str):
        """parse log line into timestamp and text"""
        if line.startswith("{"):  # assume Docker's json-file format
            data = json.loads(line)
            data["time"] = datetime.datetime.fromisoformat(data["time"])
            return data
        else:  # assume k8s-file format, https://github.com/kubernetes/design-proposals-archive/blob/main/node/kubelet-cri-logging.md
            split = line.split(maxsplit=3)
            if len(split) != 4:
                raise NotImplementedError("unsupported k8s-file log line", line)
            timestamp, stream, tags, text = split
            if "F" not in set(tags.split(":")):
                raise NotImplementedError("partial log lines not supported", line)
            return {
                "time": datetime.datetime.fromisoformat(timestamp),
                "stream": stream,
                "log": text,
            }

    def combine_perf_stacks(self, path: Path):
        pass


class AsyncDockerContainer(DockerContainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = None
        self.start_prefix.append("--detach")

    def start(self, *args, **kwargs):
        assert self.id is None, "duplicate start detected"
        ret = super().start(*args, **kwargs)
        self.id = ret.stdout.strip()
        logging.info("started container %s", self.id)

    def stop(self, timeout=-1):
        """by default wait indefinitelly until the process stops after SIGTERM"""
        assert self.id is not None, "not started yet"
        args = ["docker", "stop", "--time", str(timeout), self.id]
        return self.conn.runargs(args)

    @property
    @functools.cache
    def started_at(self):
        data = self.inspect()
        return datetime.datetime.fromisoformat(data["State"]["StartedAt"])

    @property
    @functools.cache
    def finished_at(self):
        data = self.inspect()
        if data["State"]["Running"]:
            return None
        return datetime.datetime.fromisoformat(data["State"]["FinishedAt"])

    @property
    @functools.cache
    def log_path(self):
        data = self.inspect()
        return Path(data["HostConfig"]["LogConfig"]["Path"])

    def inspect(self):
        assert self.id, "container must be started first"
        data = json.loads(self.conn.runargs(["docker", "inspect", self.id]).stdout)
        assert len(data) == 1, ("unexpected docker inspect return", data)
        return data[0]

    @property
    def pids(self):
        data = self.inspect()
        assert data["State"]["Running"], data["State"]
        return [data["State"]["Pid"]]

    @property
    def stdout_and_err(self):
        return self.conn.runargs(["docker", "logs", self.id]).stdout

    def wait(self, timeout=None):
        assert self.id, "container must be started first"
        ret = self.conn.runargs(
            ["docker", "wait", self.id], hide="stdout", timeout=timeout
        )
        value = int(ret.stdout.strip())
        if value != 0:
            raise UnexpectedExit(
                invoke.runners.Result(
                    command=self.ran_cmd, exited=value, stdout=self.stdout_and_err
                )
            )

    # TODO: timeout?
    def wait_for_line(self, regex: str):
        """
        synchronously wait until given regex appears in container log:
        use grep regex syntax
        """
        # whole file
        limit = "+0"
        ret = self.conn.run(
            cmdjoin(["tail", "-n", limit, "-f", self.log_path])
            + " | "
            + cmdjoin(["grep", "--max-count", "1", regex])
        )
        return ret


class AsyncDockerCompose(Defaults):
    defaults = {}

    def __init__(self, conn: "RemoteConnection", work_dir: Path):
        self.work_dir = work_dir
        self.conn = conn
        self.started = False

    def configure(self, **kwargs):
        super().configure(**kwargs)
        self.conn.runargs(["mkdir", "-pv", self.work_dir])
        self.params.setdefault("work_dir", self.work_dir)

    def start(self):
        self.conn.runargs(
            [
                "docker-compose",
                # "--project-name",
                # "{{ resolver }}",
                "--project-directory",
                self.work_dir,
                "up",
                "--detach",
                "--force-recreate",
                "--no-build",
                "--pull",
                "never",
                "--timestamps",
            ]
        )
        self.started = True

    def inspect_compose(self):
        assert self.started, "compose must be started first"
        ret = self.conn.runargs(
            [
                "docker-compose",
                "--project-directory",
                self.work_dir,
                "ps",
                "--all",
                "--format",
                "json",
            ]
        )
        assert (
            ret.stdout
        ), "docker-compose ps output cannot be empty (no containers defined?!)"
        out = []
        for line in ret.stdout.split("\n"):
            if not line.strip():
                continue
            out.append(json.loads(line))
        return out

    def inspect_container(self, container_id: str):
        data = json.loads(self.conn.runargs(["docker", "inspect", container_id]).stdout)
        assert len(data) == 1, ("unexpected docker inspect return", data)
        return data[0]

    @property
    def pids(self):
        data = self.inspect_compose()
        ids = [container["ID"] for container in data]
        pids = []
        for container_id in ids:
            data = self.inspect_container(container_id)
            assert data["State"]["Status"] == "running", data["State"]["Status"]
            pids.append(data["State"]["Pid"])
        return pids
