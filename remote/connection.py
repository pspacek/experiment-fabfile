import logging
from pathlib import Path
from typing import List
import uuid

import fabric
import invoke.runners
from invoke.exceptions import UnexpectedExit
import io
import jinja2
import jinja2.meta

from .args import cmdjoin
from .docker import RemoteDocker
from .fs import RemoteFS
from .git import RemoteGit


class RemoteConnection(fabric.Connection):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.git = RemoteGit(self)
        self.fs = RemoteFS(self)
        self.docker = RemoteDocker(self)

    def _prep_args(self, args):
        return cmdjoin(args)

    def runargs(self, args, **kwargs):
        return super().run(self._prep_args(args), **kwargs)

    def async_runargs(self, work_dir: Path, args: List):
        return AsyncRun(self, work_dir, args)

    def put_render_j2(self, local_path: Path, remote_path: Path, params={}):
        self.put(self._jinja2_render(local_path, params), str(remote_path))

    def _jinja2_render(self, local_template_path: Path, params: dict):
        loader = jinja2.FileSystemLoader(searchpath=local_template_path.parent)
        env = jinja2.Environment(loader=loader, undefined=jinja2.StrictUndefined)
        template = env.get_template(str(local_template_path.name))
        with open(local_template_path, "r") as template_file:
            ast = env.parse(
                source=template_file.read(), filename=str(local_template_path.name)
            )
        undefined = jinja2.meta.find_undeclared_variables(ast)
        missing = undefined - set(params.keys())
        if missing:
            logging.warning("template %s uses undefined keys: %s", local_template_path, missing)
        return io.StringIO(template.render(params))


class AsyncRun:
    def show(self):
        ret_show = self.conn.run(cmdjoin(["systemctl", "--user", "show", self.uuid]))
        lines = ret_show.stdout.rstrip("\n").split("\n")
        pairs = (line.split("=", 1) for line in lines)
        return {key: value for key, value in pairs}

    def __init__(self, conn, work_dir: Path, cmd: List[str]):
        self.conn = conn
        self.uuid = str(uuid.uuid4())
        self.work_dir = work_dir
        self.cmd = cmd
        self.full_cmd = cmdjoin(
            [
                "systemd-run",
                "--user",
                "--remain-after-exit",
                "--unit",
                self.uuid,
                "--working-directory",
                work_dir,
            ]
            + cmd
        )

        self.ran = self.conn.run(self.full_cmd)

    def is_running(self):
        return self.show()["SubState"] == "running"

    def check(self):
        state = self.show()
        if state["ActiveState"] == "failed":
            raise UnexpectedExit(
                invoke.runners.Result(
                    command=self.full_cmd,
                    exited=state["ExecMainStatus"],
                    stdout=self.output,
                )
            )

    @property
    def output(self):
        output = self.conn.run(
            cmdjoin(["journalctl", "--user", "--output", "cat", "--unit", self.uuid])
        )
        return output.stdout

    def stop(self):
        self.check()
        self.conn.run(cmdjoin(["systemctl", "--user", "stop", self.uuid]))
