from pathlib import Path
from .docker import DockerImage, AsyncDockerCompose


class Image(DockerImage):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("git_repos", ["https://github.com/DNS-OARC/dumdumd.git"])
        kwargs.setdefault("img_local_reg", "localhost/dumdumd")
        kwargs.setdefault("ref", "9f015607e545f550b839cf53696057c04db9b7ca")
        super().__init__(*args, **kwargs)
        self.git_dir = self.work_dir


class Resolver(AsyncDockerCompose):
    def __init__(self, image: Image, work_dir: Path):
        self.image = image
        super().__init__(image.conn, work_dir)

    def configure(self, **kwargs):
        super().configure(**kwargs)

        assert "version" not in self.params
        self.params["version"] = self.image.tag_or_commit

        self.conn.put_render_j2(
            Path("dumdumd/docker-compose.yaml.j2"),
            self.work_dir / "docker-compose.yaml",
            self.params
        )

    def combine_perf_stacks(self, path: Path):
        pass
