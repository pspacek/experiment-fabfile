import collections
from pathlib import Path
import shlex
from typing import List


class Inject(collections.UserString):
    """
    String which will not be automatically shell-quoted when
    sent remote for execution
    """


def cmdjoin(args: List):
    # verbose sanity check
    for idx, arg in enumerate(args):
        assert isinstance(
            arg, (str, Path, int, Inject)
        ), (f"not a string or Path, got: {type(arg)} {repr(arg)} on idx {idx}", args)
    out = []
    for arg in args:
        if isinstance(arg, Inject):
            out.append(str(arg))
        else:
            out.append(shlex.quote(str(arg)))
    return " ".join(out)


def qpath(path: Path):
    """quote Path instance into string suitable as POSIX-shell argument"""
    assert isinstance(path, Path)
    return shlex.quote(str(path))
