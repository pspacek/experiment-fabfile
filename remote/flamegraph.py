from .docker import DockerContainer, DockerImage
from .perf import PerfProfile
from .args import qpath


class FlamegraphImage(DockerImage):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            **kwargs,
            git_repos=["https://github.com/brendangregg/FlameGraph.git"],
            ref="1b1c6deede9c33c5134c920bdb7a44cc5528e9a7",
            img_local_reg="localhost/flamegraph",
        )
        self.git_dir = self.work_dir / "FlameGraph"

    def build_prep(self):
        self.conn.put("flame/Dockerfile", str(self.work_dir / "Dockerfile"))


class FlameGraph:
    def __init__(self, profile: PerfProfile):
        self.profile = profile
        self.stacks_path = None
        self.output_svg = None

        self.img = FlamegraphImage(
            self.profile.conn,
            "/tmp/build/flame",
        )
        self.img.image_prep()

    def summarize_stacks(self, separate_threads: bool):
        # should take previous output filename as input
        stacks_path = self.profile.work_dir / "stacks"
        if separate_threads:
            separate_arg = "--tid"
        else:
            separate_arg = ""
        DockerContainer(self.img, self.profile.work_dir, "flamegraph").start(
            docker_params=[
                "--rm",
            ],
            args=[
                "/bin/sh",
                "-c",
                f"stackcollapse-perf.pl {separate_arg} < {qpath(self.profile.script_path)} > {qpath(stacks_path)}",
            ],
        )
        self.stacks_path = stacks_path

    def plot(self, stacks_path=None):
        if not stacks_path:
            stacks_path = self.stacks_path
        assert stacks_path, "stacks not summarized yet"
        output_svg = self.profile.work_dir / "output.svg"

        DockerContainer(self.img, self.profile.work_dir, "flamegraph").start(
            docker_params=[
                "--rm",
            ],
            args=[
                "/bin/sh",
                "-c",
                f"flamegraph.pl --hash < {qpath(stacks_path)} > {qpath(output_svg)}",
            ],
        )
        self.output_svg = output_svg

    @property
    def output(self):
        assert self.output_svg, "SVG not generated yet"
        return self.output_svg
