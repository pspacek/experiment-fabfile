from .docker import DockerImage, AsyncDockerContainer


class Image(DockerImage):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("git_repos", ["https://gitlab.nic.cz/knot/shotgun.git"])
        kwargs.setdefault("img_local_reg", "localhost/shotgun")
        kwargs.setdefault("ref", "27fa2f5e9101047a83f8f5d32f7a9bd0e3e1007a")
        super().__init__(*args, **kwargs)
        self.git_dir = self.work_dir

class Client(AsyncDockerContainer):
    pass
