from pathlib import Path

CERT_DIR = Path(__file__).absolute().parent / ".." / "certs"


def copy(family: str, conn, target_dir: Path):
    for part in ("cert", "key"):
        local_path = CERT_DIR / f"{family}_{part}.pem"
        remote_fname = f"{part}.pem"
        conn.put(local_path, str(target_dir / remote_fname))
