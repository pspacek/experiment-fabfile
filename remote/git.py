from typing import List

from invoke.exceptions import UnexpectedExit

from .args import cmdjoin


class RemoteGit:
    def __init__(self, conn):
        self.conn = conn

    def clone_full(self, target_dir, remote: str, ref: str):
        self.conn.fs.rm_dir(target_dir)
        self.conn.run(cmdjoin(["git", "clone", remote, target_dir]))
        self.conn.run(cmdjoin(["git", "-C", target_dir, "checkout", ref]))

    def clone_shallow(self, target_dir, remote: str, ref: str):
        self.conn.fs.rm_dir(target_dir)
        self.conn.run(
            cmdjoin(
                ["git", "clone", "--branch", ref, "--depth", "1", remote, target_dir]
            )
        )

    def clone(self, target_dir, remote: str, ref: str):
        try:
            self.clone_shallow(target_dir, remote, ref)
        except UnexpectedExit:
            self.clone_full(target_dir, remote, ref)

    def describe_tag(self, repo_dir: str) -> str:
        ret = self.conn.run(
            cmdjoin(["git", "-C", repo_dir, "describe", "--tags", "--exact-match"])
        )
        return ret.stdout.strip()

    def commit_hash(self, repo_dir: str) -> str:
        ret = self.conn.run(cmdjoin(["git", "-C", repo_dir, "rev-parse", "HEAD"]))
        return ret.stdout.strip()

    def clone_and_describe(self, target_dir, remotes: List[str], ref: str):
        for remote in remotes:
            try:
                self.clone(target_dir, remote, ref)
            except UnexpectedExit:
                continue
            try:
                return self.describe_tag(target_dir)
            except UnexpectedExit:
                try:
                    return self.commit_hash(target_dir)
                except UnexpectedExit:
                    pass
        raise ValueError("could not find ref", ref, remotes)
